# Introduction
This repository include all script used to prepare and perform analysis for the PRSet paper. To facilitate reproducibility of our research, we have generated this document in the hope that we can clearly explain our codes and potentially the rationale behind each parameter. 

!!! announcement Our PRSet paper is currently under review:

Shing Wan Choi, Judit Garcia-Gonzalez, Yunfeng Ruan et al. The power of pathway-based polygenic risk scores, 28 June 2021, PREPRINT (Version 1) available at [Research Square](https://doi.org/10.21203/rs.3.rs-643696/v1)
    
## Abstract
Polygenic risk scores (PRSs) have been among the leading advances in biomedicine in recent years. 
As a proxy of genetic liability, PRSs are utilised across multiple fields and applications. 
While numerous statistical and machine learning methods have been developed to optimise their predictive accuracy3–5, all of these distil genetic liability to a single number based on aggregation of an individual’s genome-wide alleles This results in a key loss of information about an individual’s genetic profile, which could be critical given the functional sub-structure of the genome and the heterogeneity of complex disease.
Here we evaluate the performance of pathway-based PRSs, in which polygenic scores are calculated across genomic pathways for each individual, and we introduce a software, PRSet, for computing and analysing pathway PRSs.
We find that pathway PRSs have similar power for evaluating pathway enrichment of GWAS signal as the leading methods, with the distinct advantage of providing estimates of pathway genetic liability at the individual-level. 
Exemplifying their utility, we demonstrate that pathway PRSs can stratify diseases into subtypes in the UK Biobank with substantially greater power than genome-wide PRSs. 
Compared to genome-wide PRSs, we expect pathway-based PRSs to offer greater insights into the heterogeneity of complex disease and treatment response, generate more biologically tractable therapeutic targets, and provide a more powerful path to precision medicine. 

## Analysis pipeline overview
There are three type of scripts to run this analysis. 

1. [Nextflow](https://nextflow.io/) pipeline scripts, which is the main backbone for analysis
2. SQL scripts, for extracting phenotype information from the UK Biobank SQLite database
3. Rscripts, either for performing phenotype specific post-processing or for running specific analyses (e.g. [lassosum](https://github.com/tshmak/lassosum))

All the scripts are included in the folder [script](https://gitlab.com/choishingwan/prset_analyses/-/tree/master/script). Details on the Nextflow pipeline scripts can be found in the folder [docs](https://gitlab.com/choishingwan/prset_analyses/-/tree/master/docs).


## Data
We have included some data that does not require user agreement in the data folder.

As of now, the JSON files contained hard-coded path, and the script are tailored for our server. 
For users who would like to repeat our analysis, please change the path in the JSON files and also 
update the nextflow script so that it is better suited for your server.

## Dependencies
To run this pipeline, you will need the following

1. UK Biobank phenotype data in SQLite data base
    - https://gitlab.com/choishingwan/ukb_process 
2. [GreedyRelated](https://gitlab.com/choishingwan/GreedyRelated) 
    - For removing related samples, part of UK Biobank filtering pipeline
3. QCed UK Biobank data (will use 1)
    - https://gitlab.com/choishingwan/ukb-administration/-/tree/master/scripts/QC
4. [Singularity](https://singularity-tutorial.github.io/01-installation/)
5. Download the singularity container with the following command:
`singularity pull library://choishingwan/prset_analyses/prset_analyses_container:latest`
