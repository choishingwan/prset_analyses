# Introduction

This section gives details on the Nextflow scripts used to run the analyses included in our PRSet paper. Each document explains one nextflow pipeline script (e.g. <name of the nextflow script>_docs.md).

- 1_prepare_workspace_docs.md --> Documentation for nextflow script [1_prepare_workspace.nf](https://gitlab.com/choishingwan/prset_analyses/-/blob/master/script/1_prepare_workspace.nf)

- 2_real_data_analysis_docs.md --> Documentation for nextflow script [2_real_data_analysis.nf](https://gitlab.com/choishingwan/prset_analyses/-/blob/master/script/2_real_data_analysis.nf)

- 3_simulation_analysis_docs.md --> Documentation for nextflow script [3_simulation_analysis.nf](https://gitlab.com/choishingwan/prset_analyses/-/blob/master/docs/3_simulation_analysis_doc.md)

