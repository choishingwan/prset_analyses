# Preparation Script
## Purpose
The main purpose of this script is to do the following:

1. Download the latest [GO](http://geneontology.org/) and [MGI](http://www.informatics.jax.org/) data
2. Download the GTF file from [Ensembl](https://useast.ensembl.org/index.html)
3. Download the [GTEx V8](https://gtexportal.org/home/) files
4. Harmonize GMT files obtained from [MSigDB](https://www.gsea-msigdb.org/gsea/msigdb/)
5. Filter pathways based on their sizes
6. Calculate [MalaCards](https://www.malacards.org/) disease relevance scores for each pathways
7. Generate gene sets based on expression data obtained from [Skene et al](https://www.nature.com/articles/s41588-018-0129-5) and from [GTEx](https://gtexportal.org/home/)
8. Calculate LD scores for each pathway to facility LD score analyses

## Required Software

1. [Nextflow](https://nextflow.io/)
    - This is required to run the pipeline
    - Only depends on java version 8 or later

2. [Singularity](https://singularity-tutorial.github.io/01-installation/)
    - This is required so that we can used the generated container

## Required Input Files

1. Manually curated MalaCards score [download here](https://gitlab.com/choishingwan/prset_analyses/-/raw/master/data/Malacard.csv?inline=false)

2. File containing ID of samples from European ancestry in 1000 genome [download here](https://gitlab.com/choishingwan/prset_analyses/-/blob/master/data/reference/superpop_EUR.keep)

3. 1000 genome PLINK file (For LD score calculation). Need to download and processed from [1000 genome](https://www.internationalgenome.org) 
    - Not provided due to size of file

4. [MSigDB curated gene sets (gene symbols)](https://www.gsea-msigdb.org/gsea/msigdb/collections.jsp#C2). 
    - Not provided due to required user agreement

5. List of UK Biobank QCed SNPs
    - Required for LD score calculation

6. Singularity container file specifically designed for the current analysis
    - Contain all required software
    - Download using the following command:
        - `singularity pull library://choishingwan/prset_analyses/prset_analyses_container:latest`
## Data structure
We assume `nextflow` is installed in your PATH and your data are arranged in the following structure:
```
project
    |- script
    |   |- 1_prepare_workspace.nf
    |   |- modules
    |
    |- container
    |   |- prset_analyses_container_latest.sif
    |   
    |- data
        |- Malacard.csv (MalaCards information)
        |
        |- gene_sets
        |   |- raw
        |       |- MSigDB GMT files
        |
        |- reference
        |   |- superpop_EUR.keep (EUR ID in 1000G)
        |   |- ALL.chr#.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.{bed,bim,fam} (# is chromosome number)
        |
        |- genotyped
        |- ukb-qc.snplist
```

## Example Script
The following is the script used for running this analysis

!!! warning
    Due to the number of files LDSC will generate, this process require a large `ulimit` to work (i.e. `ulimit -u` > 1000)

``` bash
project=<folder containing all required data>
nextflow run \
    ${project}/script/1_prepare_workspace.nf \
    -with-singularity ${project}/container/prset_analyses_container_latest.sif \
    --malacard ${project}/data/Malacard.csv  \
    --ref ${project}/data/reference/ALL.chr#.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes \
    --pop ${project}/data/reference/superpop_EUR.keep \
    --msigdb ${project}/data/gene_sets/raw/  \
    --printsnp ${project}/data/genotyped/ukb-qc.snplist \
    --quantiles 10 \
    --wind3 10 \
    --wind5 35 \
    --min 10 \
    --max 2000
```

## Rationale
- Pathways with fewer than 10 genes or more than 2000 genes were removed to exclude over specific or too broad pathways. 
- Specificity were divided into 10 quantiles to replicate [Skene et al](https://www.nature.com/articles/s41588-018-0129-5)'s procedure
- Genic regions were extended 10kb towards 3' end and 35kb towards 5' end to include potential regulatory elements. (35kb and 10kb were selected following examples in [MAGMA](https://ctg.cncr.nl/software/magma)'s user manual)

!!! note 
    It is possible to increase the number of quantiles to any number higher than 10, but that will substantially increases the computational burden for downstream LD score analysis.

## Detail breakdown
