library(data.table)
library(magrittr)
args <- commandArgs(trailingOnly = TRUE)
# Retain only samples with non-null response to the AUDIT questions
pheno <- fread(args[1])
qc <- fread(args[2])
dropout <- fread(args[3], header = F)
cov <- fread(args[4])
out <- args[5]

get_model <- function(x) {
    paste("PC", 1:15, collapse = "+", sep = "") %>%
        paste0(x, "~Age+Sex+Centre+Batch+", .) %>%
        as.formula
}

audit_total <- get_model("AUDIT_Total")
audit_c <- get_model("AUDIT_C")
audit_p <- get_model("AUDIT_P")
pheno %>%
    .[IID %in% qc[, V2]] %>%
    .[!IID %in% dropout[, V1]] %>%
    merge(., cov, by = c("FID", "IID")) %>%
    .[, Centre := as.factor(Centre)] %>%
    .[, Batch := as.factor(Batch)] %>%
    .[, c(
        "FID",
        "IID",
        "AUDIT_Total",
        "AUDIT_C",
        "AUDIT_P",
        "Age",
        "Sex",
        "Centre",
        "Batch",
        paste("PC", 1:15, sep = "")
    )] %>%
    na.omit %>%
    .[, .(
        FID = FID,
        IID = IID,
        AUDIT_Total = lm(audit_total, data = .SD) %>%
            resid,
        AUDIT_C = lm(audit_c, data = .SD) %>%
            resid,
        AUDIT_P = lm(audit_p, data = .SD) %>%
            resid
    )] %>%
    setnames(., "AUDIT_C", "Alcohol") %>%
    fwrite(.,
           out,
           sep = "\t",
           na = "NA",
           quote = F)

# we only run AUDIT_C at the end. But the other information are available
