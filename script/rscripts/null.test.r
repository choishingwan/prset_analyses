library(genomation)
library(GenomicRanges)
library(magrittr)
gtf <- gffToGRanges("Homo_sapiens.GRCh37.75.gtf.gz") %>%
# Add 35k upstream and 10k downstream
    flank(., 35000) %>%
    flank(., 10000, start=FALSE) %>%
    reduce(.) %>%
    .[seqnames(.) %in% 1:22]

# now we try to shift the gtf by different number
kb <- 1000
mb <- kb*1000
genic.size <- ranges(gtf) %>%
    width %>%
    sum
result <- NULL 
for(i in c(2,seq(5,500,5))*mb){
    shifted.size <- shift(gtf, i) %>%
        intersect(gtf, .) %>%
        ranges %>%
        width %>%
        sum
    result %<>% rbind(., data.frame(Shift=i/mb, ratio = shifted.size/genic.size, size=shifted.size))
}

boundary <- structure(list(V1 = 1:22, V2 = c(248956422L, 242193529L, 198295559L, 
190214555L, 181538259L, 170805979L, 159345973L, 145138636L, 138394717L, 
133797422L, 135086622L, 133275309L, 114364328L, 107043718L, 101991189L, 
90338345L, 83257441L, 80373285L, 58617616L, 64444167L, 46709983L, 
50818468L)), class = "data.frame", row.names = c(NA, -22L))
num.out <- 0
shifted.gtf <- shift(gtf, 5*mb)
for(i in 1:nrow(boundary)){
    chr <- boundary[i,"V1"]
    end <- boundary[i,"V2"]
    tmp <- shifted.gtf[seqnames(shifted.gtf)==chr & end(shifted.gtf) > end]
    num.out <- num.out + length(tmp)
}