#!/usr/bin/env nextflow

////////////////////////////////////////////////////////////////////
//
//      This script is responsible to perform the simulation analyses
//
//      This script requires the GMT files, the gene location files, 
//  and the LD scores generated from the prepare_workspace script.
//  It also required BBS simulator software for simulating phenotype
//
//      On our server, if given 48 threads, this script should take 
//  around 7 hours to complete for most traits
//
////////////////////////////////////////////////////////////////////

nextflow.enable.dsl=2
params.version=false
params.help=false
version='0.0.2'
timestamp='2021-03-24'
if(params.version) {
    System.out.println("")
    System.out.println("Run Simulation Analysis - Version: $version ($timestamp)")
    exit 1
}


if(params.help){
    System.out.println("")
    System.out.println("Run Simulation Analysis - Version: $version ($timestamp)")
    System.out.println("Usage: ")
    System.out.println("    nextflow run 3_simulation_analysis.nf [options]")
    System.out.println("Mandatory arguments:")
    System.out.println("    --loc         Gene location file for MAGMA")
    System.out.println("    --gtf         GTF file for PRSet")
    System.out.println("    --gmt         Directory containing the GMT files")
    System.out.println("    --bfile       Genotype file prefix ")
    System.out.println("    --fam         QCed fam file ")
    System.out.println("    --snp         QCed SNP file ")
    System.out.println("    --cov         Covariate file")
    System.out.println("    --dropout     dropout samples")
    System.out.println("    --simperm     Number of permutations for the simulations)
    System.out.println("    --seed        seed value)
    System.out.println("    --out         output prefix)
    System.out.println("    --scores      Folder contains all precalculated ld scores")
    System.out.println("    --freq        Minor allele frequency information required by LDSC")
    System.out.println("Options:")
    System.out.println("    --wind3       Padding to 3' end ")
    System.out.println("    --wind5       Padding to 5' end")
    System.out.println("    --perm        Number of permutation for PRSet")
    System.out.println("    --xregion     Region to be excluded from the analysis ")
    System.out.println("    --thread      Number of thread use")
    System.out.println("    --start       Start count for permutation")
    System.out.println("    --help        Display this help messages")
} 

////////////////////////////////////////////////////////////////////
//                  Helper Functions
////////////////////////////////////////////////////////////////////
def fileExists(fn){
   if (fn.exists()){
       return fn;
   }else
       error("\n\n-----------------\nFile $fn does not exist\n\n---\n")
}

def gen_file(a, bgen){
    return bgen.replaceAll("@",a.toString())
}

def get_chr(a, input){
    if(input.contains("@")){
        return a
    }
    else {
        return 0
    }
}


def add_meta(meta, addition){
    def target = meta.clone()
    target.putAll(addition)
    return(target)
}

def remove_meta(meta, key){
    def target = meta.clone()
    def value = target[key]
    target.remove(key)
    return(tuple(target, value))
}

////////////////////////////////////////////////////////////////////
//                  Module inclusion
////////////////////////////////////////////////////////////////////
include {   combine_map }   from './modules/handle_meta'
include {   filter_gmt
            mock_qc
            combine_files  }   from './modules/misc'
include {   filter_summary_statistic
            filter_bed
            perform_gwas
            standardize_summary_statistic
            meta_analysis
            modify_metal
            filter_fam  }   from './modules/basic_genetic_analyses'
include {   annotate_genome
            munge_sumstats
            clean_freq
            ldsc_group_biochemical_analysis
            modify_simulated_ldsc_result  }   from './modules/calculate_ldscore'
include {   select_causal_variants
            calculate_xbeta
            simulate_phenotype
            select_samples }   from './modules/phenotype_simulation'
include {   prset_analysis
            modify_simulated_prset_result    }   from './modules/polygenic_score'
include {   magma_genotype    
            perform_annotation
            magma_sumstat
            magma_meta
            magma_gene_set
            modify_simulated_magma_output }   from './modules/magma'
include {   gather_simulation_results
            get_permutation_statistic } from './modules/generate_biochemical_sets'
////////////////////////////////////////////////////////////////////
//                  Setup Channels
////////////////////////////////////////////////////////////////////

biochemGMT = Channel.fromPath("${params.gmt}/*.gmt") 
gtf = Channel.fromPath("${params.gtf}")
genotype = Channel.fromFilePairs("${params.bfile}.{bed,bim,fam}", size:3 , flat: true){ file -> file.baseName } \
    | ifEmpty{ error "No matching plink files "} \
    | map{ a -> [ fileExists(a[1]), fileExists(a[2]), fileExists(a[3])]}
dropout = Channel.fromPath("${params.dropout}")
qcFam = Channel.fromPath("${params.fam}")
snp = Channel.fromPath("${params.snp}")
wind3 = Channel.of("${params.wind3}")
wind5 = Channel.of("${params.wind5}")
xregion = Channel.of("${params.xregion}")
geneLoc=Channel.fromPath("${params.loc}")
chromosome = Channel.value(1..22).flatten()
freq = chromosome \
    | map{ a -> [   get_chr(a, "${params.freq}"),
                    fileExists(file(gen_file(a, "${params.freq}")))]}
score = Channel.fromPath("${params.scores}/*")
// For each permutation, give them a different seed so that they will not be a duplicates of each other
def index = 0
permSeed = Channel
    .from( 1..100000 )
    .randomSample( params.simperm, params.seed )
    .map{ a-> [index++, a] }

workflow{
    // 1. Combine GMT files into one and remove all NULL pathways
    filter_gmt(biochemGMT.collect())
    // 2. Remove all ambiguous SNPs to ensure same input
    genotype_preprocessing()
    // 3. Generate simulated phenotype
    
    simulate_phenotypes(
        genotype_preprocessing.out,
        filter_gmt.out
    )
    
    // 4. perform genome wide association analysis
    
    generate_summary_statistics(
        simulate_phenotypes.out.base,
        genotype_preprocessing.out 
    )
    // 5. Run PRSet analysis
    prset_analyses(
        genotype_preprocessing.out,
        generate_summary_statistics.out,
        simulate_phenotypes.out.target,
        filter_gmt.out
    )
    // 6. Run MAGMA analysis
    magma_analyses(
        genotype_preprocessing.out,
        generate_summary_statistics.out,
        simulate_phenotypes.out.target,
        filter_gmt.out
    )
    
    // 7. Run LDSC analysis
    ldsc_analyses(
        genotype_preprocessing.out,
        generate_summary_statistics.out,
        simulate_phenotypes.out.target
    )
    // 8. Perform downstream analyses
    downstream_analyses(
        prset_analyses.out,
        magma_analyses.out,
        ldsc_analyses.out,
        simulate_phenotypes.out.rank
    )
}

workflow genotype_preprocessing{
    mock_qc(genotype)
    mock_sumstat = Channel.of("pheno") \
        | combine(mock_qc.out) 
    mock_sumstat \
        | combine(snp) \
        | combine(genotype) \
        | combine(xregion) \
        | combine(Channel.of("T")) /*Want to filter out ambiguous SNPs*/\
        | filter_summary_statistic 
    genotype \
        | combine(qcFam) \
        | combine(dropout) \
        | filter_fam
    filter_summary_statistic.out.snp \
            | combine(filter_fam.out) \
            | combine(genotype)\
            | filter_bed
    // remove the mock phenotype value
    geno = filter_bed.out \
        | map{ a -> [   a[1],
                        a[2],
                        a[3]]}
    emit:
        geno
}

workflow simulate_phenotypes{
    take: geno
    take: gmt
    main:
        nset=Channel.of(50)
        name = Channel.of(params.out)
        prefix = Channel.of("sim")
        geno \
            | map{  a -> [  1,      // fake chr
                            a[0],   // bed
                            a[1],   // bim
                            a[2]]   /* fam */ } \
            | combine(gtf) \
            | combine(gmt) \
            | combine(Channel.of("Stub")) \
            | combine(wind5) \
            | combine(wind3) \
            | combine(xregion) \
            | combine(prefix) \
            | annotate_genome \
            | map{ a -> [a[1]]} \
            | combine(permSeed) \
            | combine(nset) \
            | select_causal_variants
            
    
        //herit= Channel.of(0.1, 0.3, 0.5)
        herit = Channel.of(0.5)
        //herit = Channel.of(0.5)
        select_causal_variants.out.snps \
            | combine(geno) \
            | calculate_xbeta \
            | combine(herit) \
            | simulate_phenotype
        /*
        sample = Channel.of(["target", 1000], 
                            ["target", 100000], 
                            ["base", 100000], 
                            ["base", 250000]) */
        sample = Channel.of(["target", 100], 
                            ["base", 1000])
        simulate_phenotype.out \
            | combine(sample) \
            | select_samples
        base = select_samples.out \
            | filter{ a -> a[1] == "base"} \
            | map{  a -> [  a[0],   // meta information
                            a[2]]}  // phenotype file
        target = select_samples.out \
            | filter{ a -> a[1] == "target"} \
            | map{  a -> [  a[0],   // meta information
                            a[2]]}  // phenotype
    emit:
        base = base
        target = target
        rank = select_causal_variants.out.rank
        count = select_causal_variants.out.count
}

workflow generate_summary_statistics{
    take: base
    take: geno
    main:
        // Add size information to the meta map
        gwas = base \
            | combine(snp) \
            | combine(geno) \
            | perform_gwas \
            | standardize_summary_statistic
    emit: 
        gwas
}   

workflow prset_analyses{
    take: geno
    take: gwas
    take: target
    take: gmt
    main:
        combine_map(x: gwas, y:target, by: 0, column:["name", "herit", "numSet", "phenoName"]) \
            | combine(snp) \
            /* Explicitly state best and highRes to show intent */
            | map{ a -> def meta = a[0].clone()
                        meta["keepBest"] = "false"      // don't need best score file
                        meta["highRes"] = "false"       // don't need high resolution
                    return([    meta,                    // meta information
                                a[2],                   // target phenotype
                                a[3],                   // QC SNP
                                a[1]])}                 /* GWAS */\
            | combine(geno) \
            | combine(gtf) \
            | combine(wind5) \
            | combine(wind3) \
            | combine(Channel.of(10000)) \
            | combine(gmt) \
            | prset_analysis
        prsetRes = prset_analysis.out.summary \
            | modify_simulated_prset_result  \
            /* 
                we clone so that it doesn't change the channel upstream 
                might cause problem when resuming
            */
            | map{ a -> def meta = a[0].clone() 
                // remove PRSet specific information
                meta.remove("keepBest") 
                meta.remove("highRes")
                return ([   meta,   // meta information
                            a[1]])} // result file
    emit:
        prsetRes
}

workflow magma_analyses{
    take: geno
    take: gwas
    take: target
    take: gmt
    main:
        // 1. Generate the required annotation file for MAGMA
        
        annotate = geno \
            | map{ a -> [   "pheno",    // stud so that we can use perform_annotation from main pipeline
                            a[1]]} \
            | combine(geneLoc) \
            | combine(wind5) \
            | combine(wind3) \
            | perform_annotation \
            | map{ a -> [a[1]]}
        
        // 2. Run MAGMA on the target genotype in order to obtain the required
        //    gene based statistic
        gmagma = target \
            | combine(annotate) \
            | combine(geno) \
            | magma_genotype
        // 3. Perform MAGMA analysis on the base summary statistic (genotype is used for 
        //    LD calculation)
        smagma = gwas \
            | combine(annotate) \
            | combine(geno) \
            | magma_sumstat
            
        // 4. Meta analyzed the base and target results
        combine_map(x: gmagma, y: smagma, by: 0, column: ["name", "herit", "numSet", "phenoName"]) \
            | magma_meta
        // 5. Perform set based MAGMA analysis on the biochemical pathways
        magma_meta.out \
            | combine(gmt) \
            | magma_gene_set \
            | map{ a -> [   a[0],   // meta information
                            a[2]]}  /* result file */\
            | modify_simulated_magma_output
    emit:
        modify_simulated_magma_output.out
}

workflow ldsc_analyses{
    take: geno
    take: gwas
    take: target
    main:
        // 1. First, filter out all irrelevant sets      
        biochemical = score \
            | filter { a -> !(a =~ /Control/)} \
            | filter { a -> (a =~ /GO:/ || a =~ /MP:/ || !(a =~ /:/)) } \
            | filter { a -> !(a =~ /baseline/ || a =~ /weight/)} \
            | filter { a -> !(a =~ /NULL/)} \
            | collate(25) \
            | map{ a -> [a.toList()]}
        baseline = score \
            | filter { a -> (a =~ /baseline/) }
        weight = score \
            | filter { a -> (a =~ /weight/) }
        // 2. Perform GWAS on target data and meta-analyze with base
        frq = freq \
            | combine(baseline) \
            | clean_freq \
            | groupTuple(by: 0)

        // add the target tag at the end to ensure it doesn't have the same
        // name as the base gwas
        // Here we do group ldsc biochem, instead of re-using the biochem process, because
        // we want to limit the amount of job submission, which can blow up the pipeline
        targetGWAS = target \
            /* Rename the name element of the map to avoid file name collision */
            | map { a -> def meta = a[0].clone()
                /*  we need to store the name in a separate variable.
                    This is because once we do append here, the class of the
                    name will change to java.util.String, which differ to
                    the original GString
                */
                meta["ori"] = meta.name
                meta.name = meta.name + "-target"
                return (
                    tuple(
                        meta,
                        a[1]
                    )
                )} \
            | combine(snp) \
            | combine(geno) \
            | perform_gwas \
            | standardize_summary_statistic \
            | map {  a -> def meta = a[0].clone()
                meta.name = meta.remove("ori")
                return( [ meta,   // meta information
                            a[1]])} // summary statistic
        combine_map(x: gwas, y: targetGWAS, by: 0, column: ["name", "herit", "numSet", "phenoName"]) \
            | meta_analysis \
            | modify_metal \
            | munge_sumstats \
            | combine(baseline) \
            | combine(weight) \
            | combine(frq) \
            | combine(biochemical) \
            | ldsc_group_biochemical_analysis \
            | groupTuple(by: 0) \
            | modify_simulated_ldsc_result
    emit:
        modify_simulated_ldsc_result.out
}

workflow downstream_analyses{
    take: prsetRes
    take: magmaRes
    take: ldscRes
    take: ranking
    main:
        // Here we need to do "" around a[0] so that it will be parsed as GString, which is the
        // format for the other Channels (otherwise, it will be treated as integer)
        rankMap = ranking \
            | map{ a -> [[name: "${a[0]}"], a[1]]} 
        resultMap = prsetRes \
            | combine(magmaRes, by: 0) \
            | combine(ldscRes, by: 0)
        //resultMap.view()
        result = combine_map(x: resultMap, y: rankMap, by: 0, column: ["name"]) \
            | gather_simulation_results \
            | collect 
        combine_files(  Channel.of("simulation-result.csv"),
                        Channel.of("result"),
                        result) 
        get_permutation_statistic(
                        Channel.of("permutation-result.csv"),
                        Channel.of("result"),
                        combine_files.out)
        
}
