module load java
module load singularity
dataArk=/sc/arion/projects/data-ark/ukb/application/ukb18177/
project=/sc/arion/projects/psychgen/projects/prs/set_based_polygenic_score/

nextflow run \
    ${project}/script/5_single_trait_performance.nf \
    --sql ${dataArk}/phenotype/ukb18177.db \
    --cov ${dataArk}/phenotype/ukb18177.covar \
    --fam ${dataArk}/genotyped/ukb18177-qc.fam \
    --bfile ${dataArk}/genotyped/ukb18177 \
    --snp ${dataArk}/genotyped/ukb18177-qc.snplist \
    --gmt ${project}/analysis/preparation/data/gene_sets \
    --gtf ${project}/analysis/preparation/data/reference/Homo_sapiens.GRCh37.75.gtf.gz \
    --dropout ${dataArk}/withdrawn/w18177_20210201.csv \
    --out single_trait \
    --prsice /sc/arion/projects/psychgen/ukb/usr/sam/projects/prs/PRSice/bin/PRSice \
    --json ${project}/script/JSON/single_trait.json \
    --lassosum ${project}/script/rscripts/lassosum.R \
    -resume \
    -with-singularity ${project}/container/prset_analyses_container.sif
