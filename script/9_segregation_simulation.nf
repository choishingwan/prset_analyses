nextflow.enable.dsl=2
params.version=false
params.help=false
version='0.0.1'
timestamp='2021-04-07'
if(params.version) {
    System.out.println("")
    System.out.println("Segregation simulation analysis - Version: $version ($timestamp)")
    exit 1
}
params.wind3 = 10
params.wind5 = 35
params.xregion = "chr6:25000000-34000000"

// Most difficult part is to simulate the phenotype of interest
// Ideally, the phenotype should have high enough genetic correlation
// so that they are still the same phenotype, but have a different 
// genetic architecture
// Let's say, genetic correlation of 70%? Share 70% of causal SNPs?

genetic_correlation = Channel.of(0, 0.1, 0.5, 0.7)
heritability = Channel.of(0.1, 0.3, 0.5)
cross_validation = Channel.of(1..5) \
    | flatten
