process split_samples_for_cross_validation{
    label 'tiny'
    input:
        tuple   val(meta),
                path(pheno),
                val(fold)
    output:
        tuple   val(metaUpdate),
                path("${meta.name}-${fold}-${meta.typeAnalysis}.training"),
                path("${meta.name}-${fold}-${meta.typeAnalysis}.validate")
    script:
    metaUpdate = meta.clone()
    metaUpdate["fold"] = "${fold}"
    phenoName = meta instanceof Map ? meta.phenoName : "PhenoAdj"
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(data.table)
    pheno <- fread("${pheno}")
    pheno[Folds == ${fold}] %>%
        fwrite(., "${meta.name}-${fold}-${meta.typeAnalysis}.validate", sep="\\t")
    pheno[Folds == ${fold}, ${phenoName} := NA] %>%
        fwrite(., "${meta.name}-${fold}-${meta.typeAnalysis}.training", sep="\\t", na="NA", quote=F)
    """
}

process assign_fold_to_composite_trait{
    label 'normal'
    input:
        tuple   val(meta),
                path(phenoA),
                path(phenoB),
                val(maxFold),
                path(cov),
                path(qc),
                path(dropout)
    output:
        tuple   val(metaUpdate),
                path("${meta.name}-${meta.name2}-${meta.typeAscertain}-${meta.typeAnalysis}.csv"), emit: pheno
        path "${meta.name}-${meta.name2}-${meta.typeAscertain}-${meta.typeAnalysis}.info", emit: info
    script:
    metaUpdate = meta.clone()
    metaUpdate["phenoName"] = "PhenoAdj"
    metaUpdate["Subtype"] = "Subtype"
    // need to be careful with age when merging
    // For now, ignore covariates
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(caret)
    library(data.table)
    dropout <- fread("${dropout}", header = F)
    fam <- fread("${qc}", header = F)
    cov <- fread("${cov}", header = T)
    phenoA <-
        fread("${phenoA}")  %>%
        .[!IID %in% dropout[, V1]] %>%
        .[IID %in% fam[, V2]] %>%
        merge(., cov, by = c("FID", "IID")) %>%
        .[, c(
            "FID",
            "IID",
            "${meta.phenoName}",
            paste("PC", 1:15, sep = ""),
            "Age",
            "Sex",
            "Batch",
            "Centre"
        ), with = F]
    phenoB <-
        fread("${phenoB}")[, c("FID", "IID", "${meta.phenoName2}", "Age")]
    pheno <- merge(phenoA, phenoB, by = c("FID", "IID")) %>%
        .[, Centre := as.factor(Centre)] %>%
        .[, Subtype := NA] %>%
        .[${meta.phenoName} > 0, Subtype := 0] %>%
        .[${meta.phenoName2} > 0, Subtype := 1]
    if("${meta.typeAscertain}" == "Subtype"){
        pheno %<>%
            .[${meta.phenoName} > 0] %>%
            # Cases are defined as samples comorbid for both traits
            # Controls are defined as samples with traitA but not B
            .[, Phenotype := ${meta.phenoName2}] %>%
            .[, Subtype := Phenotype]
    } else if ("${meta.typeAscertain}" == "All") {
        pheno %<>%
            .[, Phenotype :=${meta.phenoName}+${meta.phenoName2}] %>%
            # Remove comorbid cases
            .[Phenotype <= 1, ]
    } else{
        pheno %<>%
            .[, Phenotype := ${meta.phenoName}+${meta.phenoName2}] %>%
            .[Phenotype == 1] %>%
            # Cases are defined as TraitA without TraitB
            # Controls are defined as TraitB without TraitA
            .[${meta.phenoName} == 1, Phenotype := 0]
    }
    pheno <- pheno %>%
            na.omit(., col = c("FID",
                "IID",
                paste("PC", 1:15, sep = ""),
                "Age.x",
                "Age.y",
                "Sex",
                "Batch",
                "Centre",
                "Phenotype")) %>%
            .[, PhenoAdj := paste("PC", 1:15, sep = "", collapse = "+") %>%
            paste0("Phenotype~Sex+Age.x+Age.y+Batch+Centre+", .) %>%
            as.formula %>%
            glm(., data = .SD, family = binomial) %>%
            resid]
    data.table(
        Case = sum(pheno[, Phenotype] != 0),
        Control = sum(pheno[, Phenotype] == 0),
        Ascertainment.Type = "${meta.typeAscertain}",
        TraitA = "${meta.name}",
        TraitB = "${meta.name2}",
        Analysis.Type = "${meta.typeAnalysis}"
    ) %>%
        fwrite(., "${meta.name}-${meta.name2}-${meta.typeAscertain}-${meta.typeAnalysis}.info")
    k = ${maxFold}
    if("${meta.typeAnalysis}" != "CV"){
        k = 2
    }
    pheno %>%
        .[, Folds :=  createFolds(paste(${meta.phenoName}, ${meta.phenoName2}, sep = "-"),
        k = k,
        list = FALSE)]  %>%
        fwrite(., "${meta.name}-${meta.name2}-${meta.typeAscertain}-${meta.typeAnalysis}.csv")
    """
}

process assign_fold_to_single_trait{
    label 'normal'
    input:
        tuple   val(meta),
                path(pheno),
                val(maxFold),
                path(cov),
                path(qc),
                path(dropout),
                val(binary)
    output:
        tuple   val(metaUpdate),
                path("${name}-cv.csv"), emit: pheno
        path "${name}-${meta.typeAnalysis}-cv.info", emit: info optional true
    script:
    name = meta instanceof Map? meta.name : meta
    phenoName = meta instanceof Map && meta.containsKey("phenoName")? meta.phenoName : name
    metaUpdate = meta.clone()
    processed = meta instanceof Map && meta.containsKey("processed")? meta.processed : "F"
    metaUpdate["phenoName"] = processed == "F"? "PhenoAdj" : meta.name
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(caret)
    library(data.table)
    dat <- fread("${pheno}")
    name <- "${phenoName}"
    if(!${processed}){
        dropout <- fread("${dropout}", header = F)
        fam <- fread("${qc}", header = F)
        cov <- fread("${cov}", header = T)
        dat %<>%
            .[!IID %in% dropout[, V1]] %>%
            .[IID %in% fam[, V2]] %>%
            merge(., cov, by = c("FID", "IID"))
        family <- gaussian
        name <- "PhenoAdj"
        if(${binary}){
            name <- "${meta.phenoName}"
            family <- binomial
            data.table(
                Case = sum(dat[, ${meta.phenoName}] != 0),
                Control = sum(dat[, ${meta.phenoName}] == 0),
                Sample.Type = "Single",
                TraitA = "${meta.phenoName}",
                TraitB = "${meta.phenoName}",
                Analysis.Type = "${meta.typeAnalysis}"
            ) %>%
                fwrite(., "${meta.name}-${meta.typeAnalysis}-cv.info")
        }
        dat <- dat[, c(
            "FID",
            "IID",
            "${meta.phenoName}",
            paste("PC", 1:15, sep = ""),
            "Age",
            "Sex",
            "Batch",
            "Centre"
        ), with = F] %>%
        na.omit %>%
        .[, PhenoAdj := paste("PC", 1:15, sep = "", collapse = "+") %>%
            paste0("${meta.phenoName}~Sex+Age+Batch+Centre+", .) %>%
            as.formula %>%
            glm(., data = .SD, family = family) %>%
            resid]
    }
    k <- ifelse("${meta.typeAnalysis}" == "CV", ${maxFold}, 2)
    dat[, Folds :=  createFolds(get(name),
        k = k,
        list = FALSE)]  %>%
        fwrite(., "${name}-cv.csv")
    """
}

process dichotomize_phenotype{
    label 'tiny'
    input:
        tuple   val(meta),
                path(csv)
    output:
        tuple   val(meta),
                path("${name}-binary.csv")
    script:
    name = meta instanceof Map? meta.name : meta
    phenoName = meta instanceof Map && meta.containsKey("phenoName")? meta.phenoName : name
    """
     #!/usr/bin/env Rscript
    library(magrittr)
    library(data.table)
    pheno <- fread("${csv}")
    if ("${phenoName}" == "Height") {
        # Need to generate case control for height
        extreme <-
            pheno[order(-${phenoName}), head(.SD, ceiling(.N * 5 / 100)), by = Sex]
        pheno[!is.na(${phenoName}), ${phenoName} := 0]
        pheno[IID %in% extreme[, IID] & !is.na(${phenoName}),${phenoName} := 1]
    }else if("${phenoName}" == "LDL"){
        # For LDL, we need to handle statin
        cor.factor <- pheno[Statin_Baseline == 0 & Statin_Follow == 1 &
                        !is.na(LDL_Baseline) &
                        !is.na(LDL_Follow), .(Factor = mean(LDL_Follow /
                                                                LDL_Baseline))]
        pheno %<>% .[, LDLc := LDL_Baseline] %>%
            .[Statin_Baseline == 1,
                LDLc := LDL_Baseline / cor.factor\$Factor] %>%
            .[, c("FID", "IID", "LDLc", "Centre", "Sex", "Age")] %>%
            na.omit %>%
            .[, ${phenoName} := 0] %>%
            .[LDLc > 4.9, ${phenoName} := 1]
    }
    fwrite(pheno, "${name}-binary.csv", na = "NA", quote = F)
    """
}

process process_phenotype_for_segregation{
    label "tiny"
    input:
        tuple   val(meta),
                path(csv),
                path(cov),
                path(qc),
                path(dropout)
    output:
        tuple   val(metaUpdate),
                path("${name}-adj.csv")
    script:
    name = meta instanceof Map? meta.name : name
    phenoName = meta instance of Map && meta.containsKey("phenoName")? meta.phenoName : name
    metaUpdate = meta.clone()
    metaUpdate["oriPheno"]  = "${phenoName}"
    metaUpdate["phenoName"] = "${phenoName}Adj"
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(data.table)
    # Remove dropout, extract QCed samples and merge with covariate
    dropout <- fread("${dropout}", header = F)
    fam <- fread("${qc}", header = F)
    cov <- fread("${cov}", header = T)
    pheno <- fread("${csv}")
    pheno %>%
        .[!IID %in% dropout[, V1]] %>%
        .[IID %in% fam[, V2]] %>%
        merge(., cov, by = c("FID", "IID")) %>%
        .[, c("FID",
              "IID",
              "${phenoName}",
              "Centre",
              "Batch",
              "Sex",
              "Age",
              paste("PC", 1:15, sep = "")), with = F] %>%
        .[, Batch := as.factor(Batch)] %>%
        .[, Centre := as.factor(Centre)] %>%
        na.omit %>%
        .[, ${phenoName}Adj := paste("PC", 1:15, sep = "", collapse = "+") %>%
            paste("${phenoName}~Sex+Age+Centre+Batch+", ., sep = "") %>%
            glm(., data = .SD, family = binomial) %>%
            resid] %>%
        fwrite(., "${name}-adj.csv")
    """
}

process extract_phenotype_from_sql{
    // This process is responsible for extracting the phenotypes
    // from the sqlite database using the phenotype specific sql file
    publishDir "result/phenotype", mode: 'symlink'
    label 'normal'
    input: 
        tuple   val(meta),
                path(script),
                path(rscript),
                path(sql)
    output:
        tuple   val(meta),
                path(rscript),
                path("${name}.csv")
    script:
    name = meta instanceof Map? meta.name : meta
    """
    sqlite3 ${sql} < ${script}
    """
}

process residualize_phenotypes{
    // This process will take in the phenotype specific Rscripts and run it to generate the 
    // required residualized phenotype
    label 'normal'
    input:
        tuple   val(meta),
                path(rscript),
                path(phenoFile),
                path(fam),
                path(dropout),
                path(cov)
    output:
        tuple   val(meta),
                path("${name}-adj")

    script:
    name = meta instanceof Map? meta.name : meta
    """
    Rscript ${rscript} ${phenoFile} ${fam} ${dropout} ${cov} ${name}-adj
    """
}

process preprocess_ibd_data{
    label 'normal'
    input:
        tuple   val(meta),
                path(rscript),
                path(phenoFile),
                path(cov),
                path(fam),
                path(dropout),
                val(fold)
    output:
        tuple   val(metaUpdate),
                path("${name}-sub.csv")
    script:
    name = meta instanceof Map? meta.name : meta
    ascertain = meta instanceof Map && meta.containsKey("typeAscertain")? meta.typeAscertain : "All"
    metaUpdate = meta.clone()
    metaUpdate["phenoName"] = "PhenoAdj"
    metaUpdate["Subtype"] = "Subtype"
    """
    #!/usr/bin/env Rscript
    library(data.table)
    library(magrittr)
    library(caret)
    qc <- fread("${fam}")
    dropout <- fread("${dropout}", header = F)
    cov <- fread("${cov}")
    # Ignore the IBD column from phenotype file as that does not tell us the
    # subtype of the sample of interest
    pheno <- fread("${phenoFile}") %>%
        .[ !IID %in% dropout[,V1]] %>%
        .[ IID %in% qc[,V1]] %>%
        merge(., cov, by=c("FID", "IID")) %>%
        .[, Centre := as.factor(Centre)] %>%
        # Remove comorbid cases
        .[!(!is.na(Crohns) & !is.na(Ulcerative))] %>%
        # Remove samples with IBD report but not Crohns and Ulcerative 
        .[!(!is.na(IBD) & is.na(Crohns) & is.na(Ulcerative))]
    # For supervised analysis, we use Crohns vs Ulcerative
    # For unsupervised, we use IBD case control status
    if("${ascertain}" == "Distinct"){
        # Remove controls
        pheno %<>% 
            .[!(is.na(Crohns) & is.na(Ulcerative))] %>%
            # QC to check if the age is valid
            .[, Age := pmax(Crohns, Ulcerative, na.rm=T)] %>%
            .[ Age > 0] %>%
            # We have removed comorbid and controls, so here
            # 0 = Crohns, 1 = UC
            .[, Pheno := as.numeric(is.na(Crohns))]
    }else{
        pheno %<>% 
            # Recalculate Age for cases (use age at report)
            .[! (is.na(Crohns) & is.na(Ulcerative)), Age := pmax(Crohns, Ulcerative, na.rm=T)] %>%
            # QC to check if the age is valid
            .[ Age > 0] %>%
            # Form the case control phenotype
            # 1 if sample has Crohns or Ulcerative
            .[, Pheno := as.numeric(!is.na(Crohns) | !is.na(Ulcerative))]
    }
    pheno[,Subtype := as.numeric(is.na(Crohns))]
    pheno[is.na(Crohns) & is.na(Ulcerative), Subtype := NA]
    # We don't adjust for Age because age of onset of IBD is very early, which caused a 
    # binarization in case control based on age (for supervised)
    model <- paste("PC", 1:15, collapse = "+", sep = "") %>%
        paste0("Pheno~Sex+Centre+Batch+", .) %>%
        as.formula
    pheno[, .(  FID = FID,
                IID = IID,
                PhenoAdj = glm(model, data = .SD, family=binomial) %>%
                    resid,
                Pheno = Pheno,
                Subtype = Subtype)] %>%
        .[, Folds := createFolds(paste(Pheno, Subtype , sep = "-"),
            k = ${fold},
            list = FALSE)] %>%
        fwrite("${name}-sub.csv")
    """
}
