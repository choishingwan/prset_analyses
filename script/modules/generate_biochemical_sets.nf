process generate_ensembl_map{
    // This process take in a GTF file, and generate a
    // region file required by MAGMA with the following
    // format:
    // "ID" (Ensembl ID), "chr", "start", "end", "strand", "Name" (Gene Name)
    
    // We also output the gtf file so that it will be copy to the data/reference
    // folder for later use
    publishDir "data/reference", mode: 'copy'
    
    label 'normal'
    input:
      path (gtf)
    output:
      path "Ensembl.regions", emit: ensembl
      path "${gtf}", emit: gtf
    script:
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(data.table)
    extract_attribute <- function(input, attribute) {
      strsplit(input, split = ";") %>%
        unlist %>%
        .[grepl(attribute, .)] %>%
        gsub("\\\"", "", .) %>%
        strsplit(., split = " ") %>%
        unlist %>%
        tail(n = 1) %>%
        return
    }
    # only select protein coding or gene entries
    # and extract gene id and gene name from GTF attribute field
    # Note: One thing to remember is that when using piping (%>%),
    # . represents the output from previous command. As a result of
    # that, colnames(.) means extracting the column name of the previous
    # data structure
    gtf <- fread("${gtf}") %>%
      .[V2 == "protein_coding" & V3 == "gene"] %>%
      .[, c("ID", "Name") := list(
        sapply(V9, extract_attribute, "gene_id"),
        sapply(V9, extract_attribute, "gene_name")
      )] %>%
      setnames(.,
               c(colnames(.)[1], "V4", "V5", "V7"),
               c("chr", "start", "end", "strand")) %>%
      .[, chr := as.numeric(chr)] %>%
      .[!is.na(chr)]
    
    # Get gene names that are located on more than one chromosome
    multi.chr.gene <- gtf %>%
      .[, .(chr.num = chr %>%
              unique %>%
              length), by = Name] %>%
      .[chr.num > 1, c("Name")]
    multi.map.gene <- gtf %>%
      .[, .(id.num = ID %>%
              unique %>%
              length), by = Name] %>%
      .[id.num > 1, c("Name")]
    # Now output the ensembl map
    gtf %>%
      .[!Name %in% c(multi.chr.gene[, Name], multi.map.gene[, Name])] %>%
      .[, c("ID", "chr", "start", "end", "strand", "Name")] %>%
      fwrite(., "Ensembl.regions",
             sep = "\\t")
    """
}


process generate_go_sets{
    // GO data require some formating before we can use it as it isn't in GMT format
    // This process will modify and filter the GO data to generate the required GMT file
    publishDir "data/gene_sets", mode: 'copy', pattern: '*.gmt', overwrite: true
    publishDir "data/gene_sets/dict", mode: 'copy', pattern: '*.dict', overwrite: true
    
    label 'normal'
    input:
      path ensembl
      path go
      path obo
      path malacard
      val(minSize)
      val(maxSize)
    output:
      path "GO.gmt",  emit: gmt
      path "GO.dict", emit: dict
      tuple val("ranking"), 
            path("GO.score"),  emit: score
      tuple val("count"),
            path("GO.count"),  emit: count
    script:
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(data.table)
    library(ontologyIndex)
    
    get_go_dictionary <- function(input) {
      go <- ontologyIndex::get_ontology(input)
      dict <- data.table(
        ID = go\$id,
        Name = go\$name,
        Obsolete = go\$obsolete
      ) %>%
        .[Obsolete == FALSE & grepl("^GO", ID)] %>%
        .[, Obsolete := NULL]
      fwrite(dict, "GO.dict", sep = "\\t")
      return(dict)
    }
    go.dict <- get_go_dictionary("${obo}")
    # Obtain the ensembl translation map
    ensembl <- fread("${ensembl}")
    
    # Read in the GAF
    # Retain GOs with experimental evidences and
    # not obsolete
    experimental.evidence <- c("EXP", "IDA", "IPI", "IMP", "IGI", "IEP")
    gaf <- fread("${go}") %>%
      .[V7 %in% experimental.evidence] %>%
      .[V5 %in% go.dict[, ID]]
    # Preprocess MalaCard file. For Genes with multiple score in the same phenotype
    # (this can happen when we use multiple term for a single phenotype), we take the mean
    malacard <- fread("${malacard}") %>%
      .[, .(Score = mean(Score)), by = c("Symbol", "Phenotype")]
    
    update_score <- function(name, score, num.gene) {
      if (nrow(score) > 0) {
        score_count <-
          score %>% dcast("Test" ~ Phenotype, value.var = "Score")
        num_score <-
          score %>% dcast("Test" ~ Phenotype, value.var = "Count")
        return(list(
          rank = data.table(Set = name, score_count[, -1], Size = num.gene),
          count = data.table(Set = name, num_score[, -1], Size = num.gene)
        ))
      } else{
        return(list(
          rank = data.table(Set = name, Size = num.gene),
          count = data.table(Set = name, Size = num.gene)
        ))
      }
    }
    get_pathway_string <- function(name, genes) {
      paste(genes, sep = "\\t", collapse = "\\t") %>%
        paste(name, ., sep = "\\t", collapse = "\\t") %>%
        return
    }
    # Generate the GMT file
    go.terms <- unique(gaf[, V5])
    cur.gmt <- "GO.gmt"
    fileConn <- file(cur.gmt, open = "wt")
    gene.set.rank <- NULL
    gene.set.count <- NULL
    for (i in go.terms) {
      pathway.genes <- gaf[V5 == i, "V3"] %>%
        unlist %>%
        unique
      pathway.score <-
        malacard[Symbol %in% pathway.genes, .(Score = sum(Score), Count = .N), by = Phenotype]
      num.pathway.gene <- length(pathway.genes)
      ensembl.genes <- ensembl[Name %in% pathway.genes, ID] %>%
        unique
      num.ensembl.gene <- length(ensembl.genes)
      if (num.ensembl.gene >=${minSize} & num.ensembl.gene <=${maxSize}) {
        tmp <- update_score(i, pathway.score, num.ensembl.gene)
        gene.set.rank %<>% rbind(tmp\$rank, fill = T)
        gene.set.count %<>% rbind(tmp\$count, fill = T)
        get_pathway_string(i, ensembl.genes) %>%
          writeLines(., fileConn)
        genes.without.score <-
          pathway.genes[!pathway.genes %in% malacard[, Symbol]]
        ensembl[Name %in% genes.without.score, ID] %>%
          unique %>%
          get_pathway_string(paste0(i, "_NULL"), .) %>%
          writeLines(., fileConn)
      }
    }
    close(fileConn)
    fwrite(gene.set.rank, "GO.score", sep = "\\t")
    fwrite(gene.set.count, "GO.count", sep = "\\t")
    """
}

process generate_mgi_sets{
    // MGI data require some formating before we can use it as it isn't in GMT format
    // This process will modify and filter the MGI data to generate the required GMT file
    // Specificially, we will limit the ontology level to avoid overly specific
    // gene sets
    // In addition, as Skene data were generated on mice, we will need orthologous information
    // between the Human Gene name and Mice gene name, which can be extracted from the MGI data
    publishDir "data/gene_sets", mode: 'copy', pattern: '*.gmt', overwrite: true
    publishDir "data/gene_sets/dict", mode: 'copy', pattern: '*.dict', overwrite: true
    label 'normal'
    input:
      path(ensembl)
      path(map)
      path(obo)
      path(rpt)
      path(malacard)
      val(minSize)
      val(maxSize)
    output:
        path "MGI.dict",  emit: dict
        path "human.mouse.ortholog",  emit: ortholog
        path "MGI.gmt", emit: gmt
        tuple val("ranking"),
              path("MGI.score"), emit: score
        tuple val("count"),
              path("MGI.count"), emit: count
    script:
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(data.table)
    library(ontologyIndex)
    # Only process gene set to ontology level 4 to avoid over specific gene sets
    mgi.ontology.level <- 4
    mgi <- ontologyIndex::get_ontology("${obo}")
    mgi.dict <-
      data.table(
        ID = mgi\$id,
        Name = mgi\$name,
        Obsolete = mgi\$obsolete,
        Level = sapply(mgi\$ancestors, length)
      )
    # +2 to the level because the first level is mamal phenotype and last level is itself
    mgi.dict %<>%   .[Obsolete == FALSE &
                        grepl("^MP", ID) &
                        Level < mgi.ontology.level + 2] %>%
      .[, c("Obsolete", "Level") := NULL]
    fwrite(mgi.dict, "MGI.dict", sep = "\\t")
    # Generate ortholog map
    mgi.data <- fread("${rpt}") %>%
      setnames(., c("V1", "V3", "V4"), c("Human", "Mouse", "ID"))
    human.mouse <- mgi.data[, c("Human", "Mouse")]
    fwrite(human.mouse, "human.mouse.ortholog", sep = "\\t")
    
    update_score <- function(name, score, num.gene) {
      if (nrow(score) > 0) {
        score_count <-
          score %>% dcast("Test" ~ Phenotype, value.var = "Score")
        num_score <-
          score %>% dcast("Test" ~ Phenotype, value.var = "Count")
        return(list(
          rank = data.table(Set = name, score_count[, -1], Size = num.gene),
          count = data.table(Set = name, num_score[, -1], Size = num.gene)
        ))
      } else{
        return(list(
          rank = data.table(Set = name, Size = num.gene),
          count = data.table(Set = name, Size = num.gene)
        ))
      }
    }
    
    get_gene_id <- function(name, set, map) {
      mouse.gene.id <- set[SetID == name, "ID"]
      mouse.gene.id <-  sapply(mouse.gene.id[, "ID"], function(x) {
        x %>%
          strsplit(., split = "\\\\||,") %>%
          unlist %>%
          unique
      }) %>%
        unique
      return(map[ID %in% mouse.gene.id, Human])
    }
    get_pathway_string <- function(name, genes) {
      paste(genes, sep = "\\t", collapse = "\\t") %>%
        paste(name, ., sep = "\\t", collapse = "\\t") %>%
        return
    }
    # Now generate the GMT file
    mgi.mapping <- fread("${map}") %>%
      setnames(., c("V4", "V6"), c("SetID", "ID"))
    ensembl <- fread("${ensembl}")
    malacard <- fread("${malacard}") %>%
      .[, .(Score = mean(Score)), by = c("Symbol", "Phenotype")]
    
    gene.set.rank <- NULL
    gene.set.count <- NULL
    sets <- mgi.mapping[, "SetID"] %>%
      unique %>%
      .[SetID %in% mgi.dict\$ID]
    cur.gmt <- "MGI.gmt"
    fileConn <- file(cur.gmt, open = "wt")
    
    for (i in sets[, SetID]) {
      human.genes <- get_gene_id(i, mgi.mapping, mgi.data)
      ensembl.gene <- ensembl[Name %in% human.genes, ID] %>% unique
      num.ensembl.gene <- length(ensembl.gene)
      # Malacard score has already been rank transformed
      pathway.score <-
        malacard[Symbol %in% human.genes, .(Score = sum(Score), Count = .N), by = Phenotype]
      if (num.ensembl.gene <= ${maxSize} & num.ensembl.gene >= ${minSize}) {
        tmp <- update_score(i, pathway.score, num.ensembl.gene)
        gene.set.rank %<>% rbind(tmp\$rank, fill = T)
        gene.set.count %<>% rbind(tmp\$count, fill = T)
        # we need the ensembl ID
        get_pathway_string(i, ensembl.gene) %>%
          writeLines(., fileConn)
        genes.without.score <-
          human.genes[!human.genes %in% malacard[, Symbol]]
        ensembl[Name %in% genes.without.score, ID] %>%
          unique %>%
          get_pathway_string(paste0(i, "_NULL"), .) %>%
          writeLines(., fileConn)
      }
    }
    close(fileConn)
    fwrite(gene.set.rank, "MGI.score", sep = "\\t")
    fwrite(gene.set.count, "MGI.count", sep = "\\t")
    """
}


process filter_msigdb_sets{
    // MSigDB Canonical pathways contain gene names or entrez ID. For consistencies, 
    // we convert all gene names into ensembl ID. In addition, we will remove gene sets
    // based on their sizes. We will also generate the MalaCards score excluded gene sets
    // for downstream sensitivity analyses.
    
    label 'normal'
    publishDir "data/gene_sets", mode: 'copy', pattern: '*.gmt', overwrite: true
    input:
        tuple   path(msigdb), 
                path(ensembl), 
                path(malacard),
                val(minSize),
                val(maxSize)
    output:
        path "*.gmt",   emit: gmt
        tuple val("ranking"),
              path("*.score"), emit: score
        tuple val("count"),
              path ("*.count"), emit: count
    script:
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(data.table)
    # Read in the GMT file
    # We assume the name of the database is contained within the
    # file name
    db.name <- strsplit("${msigdb}", "\\\\.") %>%
      unlist %>%
      head(n = 3) %>%
      tail(n = 1)
    update_score <- function(name, score, num.gene) {
      if (nrow(score) > 0) {
        score_count <-
          score %>% dcast("Test" ~ Phenotype, value.var = "Score")
        num_score <-
          score %>% dcast("Test" ~ Phenotype, value.var = "Count")
        return(list(
          rank = data.table(Set = name, score_count[, -1], Size = num.gene),
          count = data.table(Set = name, num_score[, -1], Size = num.gene)
        ))
      } else{
        return(list(
          rank = data.table(Set = name, Size = num.gene),
          count = data.table(Set = name, Size = num.gene)
        ))
      }
    }

    get_pathway_string <- function(name, genes) {
      paste(genes, sep = "\\t", collapse = "\\t") %>%
        paste(name, ., sep = "\\t", collapse = "\\t") %>%
        return
    }
    # We will need to read in the MSigDB file line by line as
    # each line can contain different number of elements, which R
    # doesn't like
    con <- file("${msigdb}", open = "r")
    
    msig.gmt <- file(paste0(db.name, ".gmt"), open = "wt")
    ensembl <- fread("${ensembl}")
    malacard <- fread("${malacard}") %>%
      .[, .(Score = mean(Score)), by = c("Symbol", "Phenotype")]
    
    gene.set.rank <- NULL
    gene.set.count <- NULL
    # Now go through the file and process each gene set
    
    while (length(oneLine <- readLines(con, n = 1, warn = F)) > 0) {
      pathway.genes <- strsplit(oneLine, split = "\\t") %>% unlist
      # Name of the pathway will always be the first item
      pathway.name <- as.character(pathway.genes[1]) %>%
        gsub("- ", "", .) %>%
          gsub(" ", "_", .) %>%
          gsub(":", "_", .) %>%
          gsub("-", "_", .) %>%
          gsub("\\\\(", "", .) %>%
          gsub("\\\\)", "", .) %>%
          gsub("_+", "_", .)

      # We remove the name, and also the URL (which is the second item according
      # to the specificiation)
      pathway.genes <- pathway.genes[-c(1:2)] %>%
        unique
      # Now calculate the pathway.scores
      pathway.score <-
        malacard[Symbol %in% pathway.genes, .(Score = sum(Score), Count = .N), by = Phenotype]
      ensembl.genes <- ensembl[Name %in% pathway.genes, ID]
      num.ensembl.gene <- length(ensembl.genes)
      if (num.ensembl.gene <=${maxSize} & num.ensembl.gene >=${minSize}) {
        tmp <- update_score(pathway.name, pathway.score, num.ensembl.gene)
        gene.set.rank %<>% rbind(tmp\$rank, fill = T)
        gene.set.count %<>% rbind(tmp\$count, fill = T)
        get_pathway_string(pathway.name, ensembl.genes) %>%
          writeLines(., msig.gmt)
        
        genes.without.score <- pathway.genes %>%
          .[!. %in% malacard[,Symbol]]
        ensembl[Name %in% genes.without.score, ID] %>%
          get_pathway_string(paste0(pathway.name, "_NULL"), .) %>%
          writeLines(., msig.gmt)
      }
    }
    close(con)
    close(msig.gmt)
    fwrite(gene.set.rank, paste0(db.name, ".score"), sep = "\\t")
    fwrite(gene.set.count, paste0(db.name, ".count"), sep = "\\t")
    """
}

process combine_pathway_information{
    // combine all score / counts into a single file
    
    label 'normal'

    publishDir "data/gene_sets", mode: 'copy', overwrite: true
    input: 
      tuple val(type),
            path("*")
    output:
        path("gene_set_${type}")
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(data.table)
    files <- list.files()
    res <- NULL
    for(i in files){
        res %<>% rbind(., fread(i))
    }
    fwrite(res, "gene_set_${type}", sep="\\t")
    """
}


process malacards_score_analysis{
    label 'normal'
    publishDir "result", mode: 'copy', overwrite: true
    input:
        tuple   val(meta),
                val(type),
                path(prset),
                path(magma),
                path(ldsc),
                path(malacards)
    output:
        tuple   val(meta),
                path("${name}-biochem-raw.csv"),
                path("${name}-biochem-res.csv")
    script:
    name = meta instanceof Map ? meta.name : meta
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(data.table)
    res <- fread("${ldsc}") %>%
        rbind(., fread("${magma}")) %>%
        rbind(., fread("${prset}"), fill=T)
    # remove all specificity pathways
    res <- res[, Spec := sapply(Set, function(x){ 
        strsplit(x, split=":") %>% 
        unlist %>% 
        tail(n=1) %>%
        as.numeric})] %>%
        .[is.na(Spec) | Set %like% "^MP|^GO"] %>%
        .[, -c("Spec")]
    # Check if this is the null sets
    res[, NullSet := FALSE]
    res[Set %like% "_NULL\$", NullSet := TRUE]
    # Rename null sets
    res[, Set := gsub("_NULL\$", "", Set)]
    # Get pathway database
    res[, Database := sapply(Set, function(x){
        strsplit(x, split="_") %>%
        unlist %>%
        head(n=1) %>%
        strsplit(., split=":") %>%
        unlist %>%
        head(n=1)
    })]
    # Read in MalaCards score
    malacards <- fread("${malacards}") %>%
        .[Set %in% res[,Set]]
    if("${name}"== "SCZ"){
        # This is mainly for SCZ, which I accidentally code as scz instead of SCZ
        # in the malacard score file
        setnames(malacards, "scz", "SCZ")
    }
    malacards <- malacards[, Score := (${name}) / (Size)] %>%
        .[is.na(Score), Score := 0] %>%
        .[, c("Set", "Score", "Size" )]
    biochemical <- merge(res, malacards)
    # output the raw data just in case if we need this for plotting
    fwrite(biochemical, "${name}-biochem-raw.csv")
    biochemical[, Rank := sapply(P, function(x, y) {
            1 - sum(log10(x) > log10(y)) / length(y)
        }, P), by = c("Software", "Database", "NullSet")]
    relevance <- biochemical %>%
        .[, .(
            m = mean(Score * Rank, na.rm = T)
        ), by = c("Software", "Database", "NullSet")]
    
    biochemical[, {
            model <-
            cor.test(-log10(P), Score, method = "kendall", use = "complete")
            list(
            Size = mean(Size),
            N = .N,
            tau = model\$estimate,
            p = model\$p.value
            )
        }, by = c("Software", "Database", "NullSet")] %>%
        merge(relevance) %>%   
        .[order(p,-tau)] %>%
        .[, Trait:="${name}"] %>%
        fwrite(., "${name}-biochem-res.csv")
    """
}

process prep_null_for_perm{
    publishDir "result", mode: 'copy', overwrite: true, pattern: '*matrix.csv'
    label 'normal'
    // Slight modification to normal to allow more memory
    memory '30G'
    input:
        tuple   val(meta),
                val(type),
                path(prset),
                path(magma),
                path(ldsc),
                path(ensembl),
                path(gmt),
                path(malacard),
                val(perm)
    output:
        tuple   val(meta),
                val(perm),
                path("${name}-biochem-raw-matrix.csv"),
                path("${name}-biochem-res-matrix.csv"),
                path("${name}-null.csv")
    script:
    name = meta instanceof Map ? meta.name : meta
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(data.table)
    
    # Go through the gmt file line by line to generate the reference matrix
    # First pass, check number of pathways
    con <- file("${gmt}", open = "r")
    num.path <- 0
    while (length(oneLine <- readLines(con, n = 1, warn = F)) > 0) {
        # Name of the pathway will always be the first item
        pathway.name <- strsplit(oneLine, split = "\\t") %>%
            unlist %>% 
            head(n=1)
        is.null <-  grepl("NULL", pathway.name)
        is.celltype <- grepl(":", pathway.name) & !grepl("^MP", pathway.name) & !grepl("^GO", pathway.name)
        if(!is.null & !is.celltype) num.path <- num.path+1
    }
    close(con)
    # Now, generate the required membership matrix
    ensembl <- fread("${ensembl}")
    genes <- ensembl[,ID] %>%
        unique 
    num.genes <- length(genes)
    membership <- matrix(0, nrow = num.path, ncol = num.genes)
    colnames(membership) <- genes
    rownames(membership) <- 1:nrow(membership)
    # Go through each pathway to get the membership
    con <- file("${gmt}", open = "r")
    idx <- 1 # R's index start at 1
    while (length(oneLine <- readLines(con, n = 1, warn = F)) > 0) {
        entry <- strsplit(oneLine, split = "\\t") %>%
            unlist
        pathway.name <- entry[1]
        is.null <-  grepl("NULL", pathway.name)
        is.celltype <- grepl(":", pathway.name) & !grepl("^MP", pathway.name) & !grepl("^GO", pathway.name)
        
        if(!is.null & !is.celltype){
            membership[idx, genes %in% entry] <- 1
            rownames(membership)[idx] <- entry[1]
            idx <- idx +1
        }
    }
    close(con)
    # Not worth it to remove non-pathway genes as there are just ~ 1000 of those
    # Now read in the per gene malacards score
    malacard <- fread("${malacard}") %>%
      .[, .(Score = mean(Score)), by = c("Symbol", "Phenotype")]
    if("${name}"== "SCZ"){
        # This is mainly for SCZ, which I accidentally code as scz instead of SCZ
        # in the malacard score file
        malacard <- malacard[Phenotype == "scz"]
    }else{
        malacard <- malacard[Phenotype == "${name}"]
    }
    scores <- merge(ensembl, malacard, by.x="Name", by.y="Symbol", all.x=T) %>%
        .[, c("ID", "Score")] %>%
        .[is.na(Score), Score := 0] %>%
        .[match(genes, ID)]
    obs.scores <- membership %*% scores[,Score] / rowSums(membership)
    null.scores <- replicate(${perm}, sample(scores[,Score]))
    null.pathway <- as.data.frame(membership %*% null.scores / rowSums(membership) )
    null.pathway\$Set <- row.names(null.pathway)
    colnames(null.pathway) <- 1:${perm}
    null.pathway <- as.data.table(null.pathway)
    colnames(null.pathway) <- c(1:${perm}, "Set")
    # Now read in pathway results
       res <- fread("${ldsc}") %>%
        rbind(., fread("${magma}")) %>%
        rbind(., fread("${prset}"), fill=T)
    # remove all specificity pathways
    res <- res[, Spec := sapply(Set, function(x){ 
        strsplit(x, split=":") %>% 
        unlist %>% 
        tail(n=1) %>%
        as.numeric})] %>%
        .[is.na(Spec) | Set %like% "^MP|^GO"] %>%
        .[, -c("Spec")]
    # remove null sets
    res <- res[!Set %like% "_NULL\$"]
    # Get pathway database
    res[, Database := sapply(Set, function(x){
        strsplit(x, split="_") %>%
        unlist %>%
        head(n=1) %>%
        strsplit(., split=":") %>%
        unlist %>%
        head(n=1)
    })]


    biochemical <- merge(as.data.frame(res), as.data.frame(obs.scores), by.x="Set", by.y="row.names") %>%
        as.data.table %>%
        setnames(., "V1", "Score")
    fwrite(biochemical, "${name}-biochem-raw-matrix.csv")

    biochemical[, Rank := sapply(P, function(x, y) {
            1 - sum(log10(x) > log10(y)) / length(y)
        }, P), by = c("Software", "Database")]

    relevance <- biochemical %>%
        .[, .(
            m = mean(Score * Rank, na.rm = T)
        ), by = c("Software", "Database")]
    
    biochemical.res <- biochemical[, {
            model <-
                cor.test(-log10(P), Score, method = "kendall", use = "complete")
            list(
                N = .N,
                tau = model\$estimate,
                p = model\$p.value
            )
        }, by = c("Software", "Database")] 
    biochemical.res %>%
        merge(relevance) %>%   
        .[order(p,-tau)] %>%
        .[, Trait:="${name}"] %>%
        fwrite(., "${name}-biochem-res-matrix.csv")

    # Get the null
    biochemical.null <- merge(res, null.pathway) %>%
        .[, c("Software", "Set", "Database", "P", 1:${perm}), with=F] %>% 
        melt(id.var=c("Software", "Set", "Database", "P")) %>%
        setnames(., "variable", "Perm") %>%
        setnames(., "value", "Score")
    fwrite(biochemical.null, "${name}-null.csv")
    """
}


process perform_perm_kendall{
    label 'normal'
    // Again, slightly increase the memory requirement for this job
    memory '25G'
    input:
        tuple   val(meta),
                val(perm),
                path(raw),
                path(res),
                path(permuted),
                val(total),
                val(group)
    output:
        tuple   val(meta),
                path("${name}-${group}-null.csv"),
                path("${name}-${group}-software-null.csv"),
                path("${name}-${group}-db-null.csv")
    script:
    name = meta instanceof Map ? meta.name: meta
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(data.table)
    start <-${perm} / ${total} * (${group} - 1) + 1
    end <-${perm} / ${total} * ${group}
    biochemical.null <- fread("${permuted}")[Perm >= start &
                                                 Perm <= end]
    # Trim the matrix
    biochemical.null.ref <- fread("${res}")
    biochemical.null.res <- fread("${res}") %>%
        .[, c("Software", "Database", "p", "tau")] %>%
        .[, emp.p.count := 0] %>%
        .[, Total := 0] %>%
        .[order(Database, Software)]
    software.res <- fread("${raw}") %>%
        .[, {
            model <-
                cor.test(-log10(P), Score, use = "pairwise.complete", method = "ken")
            list(p = model\$p.value,
                 tau = model\$estimate)
        }, by = c("Software", "Trait")] %>%
        .[, c("Software", "tau", "p")] %>%
        .[, emp.p.count := 0] %>%
        .[, Total := 0] %>%
        .[order(Software)]
    db.only.res <- fread("${raw}") %>%
        .[, {
            model <-
                cor.test(-log10(P), Score, use = "pairwise.complete", method = "ken")
            list(p = model\$p.value,
                 tau = model\$estimate)
        }, by = c("Database", "Trait")] %>%
        .[, c("Database", "tau", "p")] %>%
        .[, emp.p.count := 0] %>%
        .[, Total := 0] %>%
        .[order(Database)]
    
    
    get.perm.cor <- function(input, idx, group) {
        input[Perm == idx] %>%
            .[, {
                model <-
                    cor.test(-log10(P), Score, method = "kendall", use = "complete")
                list(perm.p = model\$p.value)
            }, by = group] %>%
            return
    }
    pb <- txtProgressBar(
        min = 0,
        max = end - start,
        style = 3,
        width = 100
    )
    for (i in start:end) {
        setTxtProgressBar(pb, i - start)
        tmp <-
            get.perm.cor(biochemical.null, i, c("Software", "Database")) %>%
            .[order(Database, Software)]
        # Need to account for situation where we've got NA (e.g. random into all 0)
        count <- as.numeric(!is.na(tmp[, perm.p]))
        tmp[is.na(perm.p), perm.p := 2]
        biochemical.null.res[, emp.p.count := emp.p.count + as.numeric(p >= tmp[, perm.p])] %>%
            .[, Total := Total + count]
        tmp <- get.perm.cor(biochemical.null, i, c("Software")) %>%
            .[order(Software)]
        count <- as.numeric(!is.na(tmp[, perm.p]))
        tmp[is.na(perm.p), perm.p := 2]
        software.res %>%
            .[, emp.p.count := emp.p.count + as.numeric(p >= tmp[, perm.p])] %>%
            .[, Total := Total + count]
        tmp <- get.perm.cor(biochemical.null, i, c("Database")) %>%
            .[order(Database)]
        count <- as.numeric(!is.na(tmp[, perm.p]))
        tmp[is.na(perm.p), perm.p := 2]
        db.only.res %>%
            .[, emp.p.count := emp.p.count + as.numeric(p >= tmp[, perm.p])] %>%
            .[, Total := Total + count]
    }
    fwrite(biochemical.null.res, "${name}-${group}-null.csv")
    fwrite(software.res, "${name}-${group}-software-null.csv")
    fwrite(db.only.res, "${name}-${group}-db-null.csv")     
    """
}


process combine_kendall_null{
    publishDir "result", mode: 'copy', overwrite: true
    label 'normal'
    input:
        tuple   val(meta),
                path(nullRes),
                path(softwareOnly),
                path(dbOnly)
    output:
        tuple   val(meta),
                path("${name}-biochem-perm.csv"),
                path("${name}-biochem-software-perm.csv"),
                path("${name}-biochem-db-perm.csv")
    script:
    name = meta instanceof Map ? meta.name : meta
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(data.table)
    files <- strsplit("${nullRes}", split=" ")  %>%
        unlist

    result <- fread(files[1]) %>%
        .[order(Software, Database)]
    for(i in 2:length(files)){
        tmp <- fread(files[i]) %>%
            .[order(Software, Database)]
        result[,emp.p.count := emp.p.count + tmp[,emp.p.count]]
        result[,Total := Total + tmp[,Total]]
    }
    result[,emp.p := (emp.p.count+1)/(Total+1)] %>%
        .[, Trait := "${name}"] %>%
        fwrite(., "${name}-biochem-perm.csv")
    
    files.nodb <- strsplit("${softwareOnly}", split=" ")  %>%
        unlist
    result <- fread(files.nodb[1]) %>%
        .[order(Software)]
    for(i in 2:length(files.nodb)){
        tmp <- fread(files.nodb[i]) %>%
            .[order(Software)]
        result[,emp.p.count := emp.p.count + tmp[,emp.p.count]]
        result[,Total := Total + tmp[,Total]]
    }
    result[,emp.p := (emp.p.count+1)/(Total+1)] %>%
        .[, Trait := "${name}"] %>%
        fwrite(., "${name}-biochem-software-perm.csv")
    
    files.db <- strsplit("${dbOnly}", split=" ")  %>%
        unlist
    result <- fread(files.db[1]) %>%
        .[order(Database)]
    for(i in 2:length(files.db)){
        tmp <- fread(files.db[i]) %>%
            .[order(Database)]
        result[,emp.p.count := emp.p.count + tmp[,emp.p.count]]
        result[,Total := Total + tmp[,Total]]
    }
    result[,emp.p := (emp.p.count+1)/(Total+1)] %>%
        .[, Trait := "${name}"] %>%
        fwrite(., "${name}-biochem-db-perm.csv")
    """
}

process get_permutation_statistic{
    label 'normal'
    publishDir "${dir}", mode: 'copy'
    input:
        val(name)
        val(dir)
        path(res)
    output:
        path("${name}")
    script:
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(data.table)
    fread("${res}") %>%
        .[, {
            model <-
                cor.test(Ratio, -log10(P), use = "pairwise.complete", method = "kendall")
            list(p = model\$p.value,
                tau = model\$estimate)
            }, by = c("Perm", "Software", "NumSet", "Heritability", "BaseSize", "TargetSize")] %>%
        fwrite(., "${name}")
    """

}

process gather_simulation_results{
    label 'normal'
    input:
        tuple   val(meta),
                path(prset),
                path(magma),
                path(ldsc),
                path(rank)
    output:
        path "*-raw.csv"
    script:
    """
    #!/usr/bin/env Rscript
    library(magrittr)
    library(data.table)
    res <- fread("${ldsc}") %>%
        rbind(., fread("${magma}")) %>%
        rbind(., fread("${prset}"), fill=T)
    # Get pathway database
    res[, Database := sapply(Set, function(x){
        strsplit(x, split="_") %>%
        unlist %>%
        head(n=1) %>%
        strsplit(., split=":") %>%
        unlist %>%
        head(n=1)
    })]
    emp.rank <- fread("${rank}") %>%
        setnames(., "Name", "Set") %>%
        .[, Ratio := Index / Total]
    raw <- merge(res, emp.rank) %>%
        .[, Perm := Perm]
    fwrite(raw, "${meta.name}-${meta.numSet}-${meta.herit}-${meta.targetSize}-${meta.baseSize}-raw.csv")
    """
}
