process lassosum_analysis{
    label 'prsice'
    afterScript 'rm *bed'
    clusterOptions = '-P acc_psychgen -M 49152 -R select[mem>=2048] rusage[mem=2048]'
    input:
        tuple   val(meta),
                path(pheno),
                path(snp),
                path(gwas),
                path(bed),
                path(bim),
                path(fam),
                path(lassosum)
    output:
        tuple   val(meta),
                path("${name}-lassosum-best.gz"), emit: best optional true
        tuple   val(meta),
                path("${name}-lassosum-all.gz"), emit: all optional true

    script:
    name = meta instanceof Map? meta.name : meta
    phenoName = meta instanceof Map && meta.containsKey("phenoName")? meta.phenoName : name
    all = meta instanceof Map && meta.containsKey("allScore")? meta.allScore : "false"
    best = meta instanceof Map && meta.containsKey("best")? meta.best : "false"
    base=bed.baseName
    """
    # Reduce genotyep file size, which helps to speed up lassosum rather significantly
    plink \
        --bfile ${base} \
        --keep ${pheno} \
        --extract ${snp} \
        --make-bed \
        --out genotype
    
    all=""
    if [ "${all}" == "true" ]; then
        all="--all"
    fi
    score=""
    if [ "${best}" == "true" ]; then
        score="--score"
    fi
    Rscript ${lassosum} \
        --target genotype \
        --base ${gwas} \
        --snp SNP \
        --effective A1 \
        --reference A2 \
        --stat BETA \
        --pvalue P \
        --out ${name}-lassosum \
        --thread ${task.cpus} \
        --pheno ${pheno} \
        --name ${phenoName} \
        --train ${pheno} \
        --Ncol N \
        \${score} \
        \${all}
    if [ "${best}" == "true" ]; then
        gzip ${name}-lassosum-best 
    fi
    if [ "${all}" == "true" ]; then
        gzip ${name}-lassosum-all
    fi
    """
}

process prsice_analysis{
    label 'prsice'
    afterScript "rm *.prsice"
    publishDir "result/prs/", mode: 'symlink', pattern: "*best.gz"
    input:
        tuple   val(meta),
                path(pheno),
                path(snp),
                path(gwas),
                path(bed),
                path(bim),
                path(fam)
    output:
        tuple   val(meta),
                path("${name}.summary"), emit: summary optional true
        tuple   val(meta),
                path("${name}.best.gz"), emit: best optional true
        tuple   val(meta),
                path("${name}.all_score.gz"), emit: all optional true
    script:
    prefix = bed.baseName
    name = meta instanceof Map? meta.name : meta
    phenoName = meta instanceof Map && meta.containsKey("phenoName")? meta.phenoName : name
    allScore = meta instanceof Map && meta.containsKey("allScore")? meta.allScore : "false"
    
    """
    all=""
    if [ "${allScore}" == "true" ]; then
        all="--no-regress --non-cumulate --inter 1e-3"
    fi
    PRSice \
        --base ${gwas} \
        --snp SNP  \
        --a1 A1 \
        --a2 A2 \
        --stat BETA \
        --pvalue P \
        --thread ${task.cpus} \
        --target ${prefix} \
        --keep ${pheno} \
        --pheno ${pheno} \
        --pheno-col ${phenoName} \
        --out ${name} \
        --ultra \
        --beta \
        --extract ${snp} \
        \${all}
    if [ "${allScore}" == "true" ]; then
        gzip *all_score
    else
        gzip *best
    fi
    """
}

process prset_analysis{
    // This process is responsible for running the PRSet analysis. 
    // If keepBest is false, we will delete the best file, otherwise,
    // we will gzip the best file and provide it as an output
    label "prset"
    afterScript "rm *.prsice"
    input:
        tuple   val(meta),
                path(pheno),
                path(snp),
                path(gwas),
                path(bed),
                path(bim),
                path(fam),
                path(gtf),
                val(wind5),
                val(wind3),
                val(perm),
                path(gmt)
    output:
        tuple   val(meta),
                path("${name}.summary"), emit: summary
        tuple   val(meta),
                path("${name}.best.gz"), emit: best optional true
        tuple   val(meta),
                path("${name}.snp"), emit: snp optional true
    stub:
    """
    touch ${name}.summary
    if [[ "${keepBest}" != "false" ]]; then
        touch ${name}.best
    fi
    """
    script:
    name = meta
    highRes = "false"
    best = "false"
    phenoName = name
    if(meta instanceof Map){
        name = meta.name
        best = meta.containsKey("keepBest") ? meta.best : "false"
        highRes = meta.containsKey("highRes") ? meta.highRes : "false"
        phenoName = meta.containsKey("phenoName") ? meta.phenoName : name
    }
    prefix=bed.baseName
    """
    wind5_com=""
    wind3_com=""
    if [ ! -z "${wind5}" ]; then
        wind5_com="--wind-5 "${wind5}kb
    fi
    if [ ! -z "${wind3}" ]; then
        wind3_com="--wind-3 "${wind3}kb
    fi
    # only do permutation if we are not doing high resolution prset
    highRes="--set-perm ${perm}"
    if [ "${highRes}" == "true" ]; then
        highRes="--upper 0.5"
    fi
    PRSice \
        --base ${gwas} \
        --target ${prefix} \
        --out ${name} \
        --keep ${pheno} \
        --extract ${snp} \
        --pheno ${pheno} \
        --pheno-col ${phenoName} \
        --snp SNP \
        --pvalue P \
        --beta \
        --stat BETA \
        --a1 A1 \
        --a2 A2 \
        --gtf ${gtf} \
        --msigdb ${gmt} \
        --thread ${task.cpus} \
        --ultra \
        --print-snp \
        \${wind5_com} \
        \${wind3_com} \
        \${highRes}

    if [[ "${best}" == "false" ]]; then
        rm ${name}.best
    else
        gzip *best
    fi
    """
}

process modify_prset_result{
    label 'normal'
    input:
        tuple   val(meta),
                path(summary)
    output:
        tuple   val(meta),
                val("Set"),
                path("${name}-Set-prset.csv")
    script:
    name = meta instanceof Map? meta.name : meta
    """
    #!/usr/bin/env Rscript
    library(data.table)
    library(magrittr)
    fread("${summary}") %>%
        .[, Competitive.P := as.numeric(Competitive.P)] %>%
        .[!is.na(Competitive.P)] %>%
        .[, c("Set", "Coefficient", "Competitive.P", "P")] %>%
        setnames(., "P", "Self.P") %>%
        setnames(., "Competitive.P", "P") %>%
        .[, Software := "PRSet"] %>%
        .[, Type := "Set"] %>%
        .[, Trait := "${name}"] %>%
        fwrite(., "${name}-Set-prset.csv")
    """
}

process modify_simulated_prset_result{
    label 'normal'
    input:
        tuple   val(meta),
                path(summary)
    output:
        tuple   val(meta),
                path("*-prset.csv")
    script:
    """
    #!/usr/bin/env Rscript
    library(data.table)
    library(magrittr)
    fread("${summary}") %>%
        .[, Competitive.P := as.numeric(Competitive.P)] %>%
        .[!is.na(Competitive.P)] %>%
        .[, c("Set", "Coefficient", "Competitive.P", "P")] %>%
        setnames(., "P", "Self.P") %>%
        setnames(., "Competitive.P", "P") %>%
        .[, Software := "PRSet"] %>%
        .[, Perm := "${meta.name}"] %>%
        .[, NumSet := "${meta.numSet}"] %>%
        .[, Heritability := "${meta.herit}"] %>%
        .[, TargetSize := "${meta.targetSize}"] %>%
        .[, BaseSize := "${meta.baseSize}"] %>%
        fwrite(., "${meta.name}-${meta.numSet}-${meta.herit}-${meta.targetSize}-${meta.baseSize}-prset.csv")
    """
}

process extract_significant_gmt{
    label 'tiny'
    input:
        tuple   val(meta),
                path(summary),
                path(gmt)
    output:
        tuple   val(meta),
                path("trim_gmt")
    script:
    """
    awk 'NR == FNR && FNR > 2 && \$12< 0.05 { a[\$2]=1} NR != FNR && \$1 in a {print}' ${summary} ${gmt} > trim_gmt
    """
}
