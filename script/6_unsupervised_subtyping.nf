// Try to perform unsupervised subtyping using IBD, which have known
// subtypes that we can use for validation
nextflow.enable.dsl=2
params.version=false
params.help=false
version='0.0.1'
timestamp='2021-05-18'
if(params.version) {
    System.out.println("")
    System.out.println("Unsupervised subtyping - Version: $version ($timestamp)")
    exit 1
}
params.wind3 = 10
params.wind5 = 35
params.perm = 10000


if(params.help){
    System.out.println("")
    System.out.println("Unsupervised subtyping - Version: $version ($timestamp)")
    System.out.println("Usage: ")
    System.out.println("    nextflow run 8_unsupervised_subtyping.nf [options]")
	System.out.println("Mandatory arguments:")
    System.out.println("    --bfile       UK biobank genotype file ")
    System.out.println("    --fam         QCed fam file ")
    System.out.println("    --snp         QCed SNP file ")
    System.out.println("    --out         Output prefix ")
    System.out.println("    --gmt         Folder containing the GMT files")
    System.out.println("    --gtf         GTF reference file")
    System.out.println("    --sql         UK biobank phenotype data base")
    System.out.println("    --prsice      PRSice executable")
    System.out.println("    --json        JSON contain phenotype information")
    System.out.println("    --dropout     Drop out samples")
    System.out.println("    --lassosum    lassosum Rscript")
    System.out.println("    --cov         Covariate with 40 PCs and batch")
    System.out.println("Options:")
    System.out.println("    --wind3       Padding to 3' end ")
    System.out.println("    --wind5       Padding to 5' end")
    System.out.println("    --help        Display this help messages")
}


////////////////////////////////////////////////////////////////////
//                  Helper Functions
////////////////////////////////////////////////////////////////////
def fileExists(fn){
   if (fn.exists()){
       return fn;
   }else
       error("\n\n-----------------\nFile $fn does not exist\n\n---\n")
}

def gen_file(a, bgen){
    return bgen.replaceAll("@",a.toString())
}

def get_chr(a, input){
    if(input.contains("@")){
        return a
    }
    else {
        return 0
    }
}

////////////////////////////////////////////////////////////////////
//                  Module inclusion
////////////////////////////////////////////////////////////////////
include {   combine_map
            addMeta
            removeMeta  }   from './modules/handle_meta'
include {   filter_gmt
            combine_files as combine_unsupervised
            combine_files as combine_supervised }   from './modules/misc'
include {   extract_phenotype_from_sql
            preprocess_ibd_data
            split_samples_for_cross_validation  }   from './modules/phenotype_extraction'
include {   standardize_summary_statistic
            filter_summary_statistic   }   from './modules/basic_genetic_analyses'
include {   prset_analysis
            prset_analysis as highResPRSet
            extract_significant_gmt
            prsice_analysis
            prsice_analysis as allScorePRSice   
            lassosum_analysis   }   from './modules/polygenic_score'
include {   unsupervised_classification_with_set
            unsupervised_classification_with_single_prs
            supervised_pathway_classification
            supervised_classification_with_single_prs  }   from './modules/classification.nf'
////////////////////////////////////////////////////////////////////
//                  Setup Channels
////////////////////////////////////////////////////////////////////


// Trying to parse the JSON input
import groovy.json.JsonSlurper
def jsonSlurper = new JsonSlurper()
// new File object from your JSON file
def ConfigFile = new File("${params.json}")
// load the text from the JSON
String ConfigJSON = ConfigFile.text
// create a dictionary object from the JSON text
def phenoConfig = jsonSlurper.parseText(ConfigJSON)
// Parse the summary statistic meta data
sumstat = Channel.from(phenoConfig.collect{ content ->
    [   [   name: content.name,         // organize this into map
            phenoName: content.phenoName], 
        file(content.gwas), 
        content.rsid,
        content.a1,
        content.a2,
        content.statistic,
        content.pvalue,
        content.se,
        content.ncol,
        content.sampleSize,
        content.isBeta
    ]})
xregions = Channel.from(phenoConfig.collect{ content ->
    [   [   name: content.name,         // organize this into map
            phenoName: content.phenoName],
        content.xregion
    ]})
// Parse the phenotype meta data
pheno = Channel.from(phenoConfig.collect{ content ->
    [   [   name: content.name,         // organize this into map
            phenoName: content.phenoName],
        file(content.phenoFile),
        file(content.gwas)   // this is a stub, to replace the rscript which isn't provided here
    ]}) \
    | filter{ a -> a[0].name == "IBD"}

biochemGMT = Channel.fromPath("${params.gmt}/*.gmt") 
gtf = Channel.fromPath("${params.gtf}")
genotype = Channel.fromFilePairs("${params.bfile}.{bed,bim,fam}", size:3 , flat: true){ file -> file.baseName } \
    | ifEmpty{ error "No matching plink files "} \
    | map{ a -> [ fileExists(a[1]), fileExists(a[2]), fileExists(a[3])]}
sql = Channel.fromPath("${params.sql}")
dropout = Channel.fromPath("${params.dropout}")
qcFam = Channel.fromPath("${params.fam}")
snp = Channel.fromPath("${params.snp}")
wind3 = Channel.of("${params.wind3}")
wind5 = Channel.of("${params.wind5}")
cov = Channel.fromPath("${params.cov}")
lassosum = Channel.fromPath("${params.lassosum}")
fold = Channel.of(1..10)

workflow{
    filter_gmt(biochemGMT.collect())
    phenotype_preprocessing()
    sumstat_preprocessing()
    
    polygenic_analysis(
        phenotype_preprocessing.out,
        sumstat_preprocessing.out,
        filter_gmt.out
    )
}

workflow phenotype_preprocessing{
    // Generate the required data 
    pheno \
        | combine(sql) \
        | extract_phenotype_from_sql \
        | combine(Channel.of("All", "Distinct")) \
        | map{ a -> def meta = a[0].clone()
                    meta["typeAscertain"]  = a[3]
                    return([
                                meta,   // meta information
                                a[1],   // STUB
                                a[2]    // Phenotype file 
                    ])} \
        | combine(cov) \
        | combine(qcFam) \
        | combine(dropout) \
        | combine(fold.max()) \
        | preprocess_ibd_data \
        | combine(fold) \
        | split_samples_for_cross_validation
        
    emit:
        split_samples_for_cross_validation.out
}

workflow sumstat_preprocessing{
    sumstat \
        | standardize_summary_statistic \
        | combine(snp) \
        | combine(genotype) \
        | combine(xregions, by: 0) \
        | combine(Channel.of("F")) \
        | filter_summary_statistic
    emit:
        filter_summary_statistic.out.sumstat
}

workflow polygenic_analysis{
    take: samples
    take: gwas
    take: gmt
    main:
        // we can map the summary statistics with the phenotypes
        input = samples \
            | combine(gwas) \
            | combine(snp) \
            | map{  a -> def meta = a[0].clone()
                        meta.name = a[3].name
                        return([
                            meta,   // meta information 
                            a[1],   // training
                            a[5],   // QCed SNPs
                            a[4]    // sumstat
                        ])}
        // 1. Run PRSet analysis just like before to get the PRS
        input \
            | combine(genotype) \
            | combine(gtf) \
            | map{ a -> addMeta(x: a, meta:["keepBest":"true", "highRes":"false"])} \
            | combine(wind5) \
            | combine(wind3) \
            | combine(Channel.of(10000)) \
            | combine(gmt) \
            | prset_analysis
        significantGMT = prset_analysis.out.summary \
            | combine(gmt) \
            | extract_significant_gmt \
            | map{ a -> removeMeta(x: a, keys: ["highRes", "keepBest"])}
        input \
            | combine(significantGMT, by: 0) \
            | map{ a -> addMeta(x: a, meta:["keepBest":"true", "highRes":"true"])} \
            | combine(genotype) \
            | combine(gtf) \
            | map{ a -> def meta = a[0].clone()
                        meta["oriName"] = meta["name"]
                        meta["name"] = meta["name"]+"-hybrid"
                        return([
                            meta,   // meta information
                            a[1],   // target phenotype
                            a[2],   // QCed SNP
                            a[3],   // Sumstat
                            a[5],   // bed
                            a[6],   // bim
                            a[7],   // fam
                            a[8],   // gtf
                            "${params.wind5}",   // wind5
                            "${params.wind3}",  // wind3
                            0,      // Perm
                            a[4]    // GMT
                        ])
                        } \
            | highResPRSet
        highRes = highResPRSet.out.summary \
            | combine(highResPRSet.out.best, by: 0) \
            | combine(highResPRSet.out.snp, by: 0) \
            | map{ a -> def meta = a[0].clone()
                        meta.remove("name")
                        meta["name"] = meta.remove("oriName")
                        meta.remove("keepBest")
                        meta.remove("highRes")
                        return([
                            meta,
                            a[1],   // summary
                            a[2],   // best
                            a[3]    /* SNPs */ ])} \
            | combine(Channel.of("PRSet-hybrid"))
        prsRes = prset_analysis.out.summary \
            | combine(prset_analysis.out.best, by: 0) \
            | combine(prset_analysis.out.snp, by: 0) \
            | map{ a -> removeMeta(x: a, keys: ["keepBest", "highRes"])} \
            | combine(Channel.of("PRSet")) \
            | mix(highRes) 
        samples \
            | combine(gwas) \
            | map{  a -> def meta = a[0].clone()
                        meta.name = a[3].name
                        return([
                            meta,   // meta information 
                            a[1],   // training
                            a[2],   // validated
                        ])} \
            | combine(prsRes, by: 0) \
            | (supervised_pathway_classification & unsupervised_classification_with_set)
            
        // 2. Run PRSice to get the PRSs
        input \
            | map{ a -> addMeta(x: a, meta: ["allScore": "false"])} \
            | combine(genotype) \
            | prsice_analysis
        input \
            | map{ a -> addMeta(x: a, meta: ["allScore": "true"])} \
            | combine(genotype) \
            | allScorePRSice
        // 2a. Combine PRSice results
        allScore = allScorePRSice.out.all  \
            | map{ a -> removeMeta(x: a, keys: ["allScore"])}
        prsiceRes = prsice_analysis.out.best \
            | map{ a -> removeMeta(x: a, keys: ["allScore"])} \
            | combine(allScore, by: 0) \
            | combine(Channel.of("PRSice"))
        // 3. Run lassosum
        input \
            | map{ a -> addMeta(x: a, meta: ["allScore": "true", "best": "true"])} \
            | combine(genotype) \
            | combine(lassosum) \
            | lassosum_analysis
        // 3a. Combine lassosum results with PRSice's results
        singlePRS = lassosum_analysis.out.best \
            | combine(lassosum_analysis.out.all, by: 0) \
            | map{ a -> removeMeta(x: a, keys: ["allScore", "best"])} \
            | combine(Channel.of("lassosum")) \
            | mix(prsiceRes)
        samples \
            | combine(gwas) \
            | map{  a -> def meta = a[0].clone()
                        meta.name = a[3].name
                        return([
                            meta,   // meta information 
                            a[1],   // training
                            a[2],   // validated
                        ])} \
            | combine(singlePRS, by: 0) \
            | (supervised_classification_with_single_prs & unsupervised_classification_with_single_prs)
            
            unsupervised = unsupervised_classification_with_single_prs.out \
                | mix(unsupervised_classification_with_set.out)\
                | collect
            supervised = supervised_classification_with_single_prs.out \
                | mix(supervised_pathway_classification.out.res) \
                | collect

            combine_supervised("${params.out}-supervised.csv", "result", supervised)
            combine_unsupervised("${params.out}-unsupervised.csv", "result", unsupervised)
                
}
