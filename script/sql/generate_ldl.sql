.mode csv
.header on 
.output LDL.csv
SELECT  s.sample_id AS FID,
        s.sample_id AS IID, 
        age.pheno AS Age,
        sex.pheno AS Sex,
        centre.pheno AS Centre,
        MAX(
            CASE WHEN med.instance = 0 THEN 
                CASE
                WHEN
                    med.pheno in (1141146234, 1141192414, 1140910632,
                                    1140888594, 1140864592, 1141146138,
                                    1140861970, 1140888648, 1141192410,
                                    1141188146, 1140861958, 1140881748,
                                    1141200040, 1140861922)
                THEN 1
                ELSE 0
                END
            END
        ) AS Statin_Baseline,
        MAX(
            CASE WHEN med.instance = 1 THEN 
                CASE
                WHEN
                    med.pheno in (1141146234, 1141192414, 1140910632,
                                    1140888594, 1140864592, 1141146138,
                                    1140861970, 1140888648, 1141192410,
                                    1141188146, 1140861958, 1140881748,
                                    1141200040, 1140861922)
                THEN 1
                ELSE 0
                END
            END
        ) AS Statin_Follow,
        MAX(
            CASE WHEN med.instance = 0 THEN 
                CASE
                WHEN
                    med.pheno in (1140861958, 1140888594, 1140888648,
                                    1141146234, 1141192410, 1140861922,
                                    1141146138)
                THEN 1
                ELSE 0
                END
            END
        ) AS Special_Baseline,
        MAX(
            CASE WHEN med.instance = 1 THEN 
                CASE
                WHEN
                    med.pheno in (1140861958, 1140888594, 1140888648,
                                    1141146234, 1141192410, 1140861922,
                                    1141146138)
                THEN 1
                ELSE 0
                END
            END
        ) AS Special_Follow,
        fasting.pheno AS Fasting,
        dilution.pheno AS Dilution,
        MAX(
            CASE WHEN ldl.instance = 0 THEN ldl.pheno END
        ) AS LDL_Baseline,
        MAX(
            CASE WHEN ldl.instance = 1 THEN ldl.pheno END
        ) AS LDL_Follow
FROM    Participant s 
        JOIN f20003 med ON 
            s.sample_id=med.sample_id 
        LEFT JOIN f30780 ldl ON
            s.sample_id=ldl.sample_id
        LEFT JOIN f31 sex ON
            s.sample_id=sex.sample_id 
            AND sex.instance = 0
        LEFT JOIN f21003 age ON 
            s.sample_id=age.sample_id 
            AND age.instance = 0
        LEFT JOIN f54 centre ON 
            s.sample_id=centre.sample_id 
            AND centre.instance = 0
        LEFT JOIN f189 ses ON 
            s.sample_id=ses.sample_id 
            AND ses.instance = 0
        LEFT JOIN f74 fasting ON
            s.sample_id=fasting.sample_id
            AND fasting.instance = 0
        LEFT JOIN f30897 dilution ON
            s.sample_id=dilution.sample_id
            AND dilution.instance = 0
GROUP BY s.sample_id;
.quit
